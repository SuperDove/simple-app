/* DOM scope
   DOM global variables:
    - window
    - document
*/

// Variable declarations:
var world = 'World',
  simple = {
    name: 'Light'
};

let i = 0;
const lightSpeed = 300000;



;(function(window, document, console) {
  'use strict';
  console.clear();

  // Class User
  var User = (function() {
    // Class Constuctor
    function User(name) {
      this.initialize(name);
    }

    User.prototype.initialize = function(name) {
      this.name = name;
    };

    // Public method
    User.prototype.setName = function(el) {
      var el = document.querySelector(el);

      el.innerHTML = this.name;
    };

    User.prototype.talk = function(el) {
      var el = document.querySelector(el),
        input = el.querySelector('input'),
        output = el.querySelector('.user-answer');

      input.addEventListener('input', function() {
        if (this.value == "What's up?") {
          output.innerHTML = "I'm great! Thanks! =)";
          output.style.opacity = 1;

          setTimeout(function() {
            output.style.opacity = 0;

            setTimeout(function() {
              output.innerHTML = '';
            }, 500);
          }, 5000);
        }
      });
    };

    return User;
  })();

  var worldUser = new User(world);

  window.onload = function() {
    worldUser.setName('.user-name');
    worldUser.talk('.talk-to-user');
  };

})(window, document, console);












